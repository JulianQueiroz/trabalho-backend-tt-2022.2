const Laptop = require('../models/Laptop');
const { response } = require('express');

const create = async(req,res) => {
    try{
        const user = await Laptop.create(req.body);
        return res.status(201) .json({message:"Produto registrado.", user:user});
    }
    catch(err) {

        return res.status(500).json({error:err});
    }
};
const index = async(req,res) => {
    try {
        const users = await Laptop.findAll();
        return res.status(200).json({users});
    }catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await Laptop.findByPk(id);
        return res.status(200).json({user});
    }catch(err){
        return res.status(500).json({err});
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Laptop.update(req.body, {where: {id: id}});
        if(updated) {
            const user = await Laptop.findByPk(id);
            return res.status(200).send(user);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Produto não encontrado");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Laptop.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Produto deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Produto não encontrado.");
    }
};
const addRelationship = async(req,res) => {
    const {id} = req.params;
    try {
        const laptop = await Laptop.findByPk(id);
        const user = await User.findByPk(req.body.UserId);
        await laptop.setUser(User);
        return res.status(200).json(Laptop.findByPk(id));
    }catch(err){
        return res.status(500).json({err});
    }
};
const removeRelationship = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await Laptop.findByPk(id);
        await user.setRole(null);
        return res.status(200).json(user);
    }catch(err){
        return res.status(500).json({err});
    }
}

module.exports = {
    index,
    show,
    create,
    update,
    destroy
};