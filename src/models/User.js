const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");
// atributos de User
const User = sequelize.define('User',{
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    adress: {
        type: DataTypes.STRING,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    birthday: {
        type: DataTypes.DATEONLY,
        allowNull:false
    }

});
// Relação 1:N
User.associate = function(models) {
    User.hasMany(models.Laptop)
};

module.exports = User;