const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");
// atributos de Laptop
const Laptop = sequelize.define('Laptop', {

    condition:{
        type: DataTypes.STRING,
        allowNull: false
    },
    type:{
        type: DataTypes.STRING,
        allowNull: false
    },
    name:{
        type: DataTypes.STRING,
        allowNull: false

    },
    price: {
        type: DataTypes.DOUBLE,
        allowNull:false
    },
    quantity:{
        type: DataTypes.INTEGER,
        allowNull: false
    }
});
// Relação 1:N
Laptop.associate = function(models) {
    Laptop.belongsTo(models.User);
};

module.exports = Laptop