// rotas para Usuário
const { Router } = require('express');
const UserController = require('../controllers/UserController');
const router = Router();

router.post('/User', UserController.create);
router.get('/index_user', UserController.index);
router.get('/Users/:id', UserController.show);
router.put('/User/:id', UserController.update);
router.delete('/User_delete/:id', UserController.destroy);

module.exports=router;

// rotas para Laptop
const LaptopController = require('../controllers/LaptopController');

router.post('/Laptop', LaptopController.create);
router.get('/index_laptop', LaptopController.index);
router.get('/Laptops/:id', LaptopController.show);
router.put('/Laptop/:id', LaptopController.update);
router.delete('/Laptop_delete/:id', LaptopController.destroy);

module.exports=router;